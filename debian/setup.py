from setuptools import setup
from distutils.core import setup
import glob

setup(
	name = 'GaussSum',
	version = '@DEB_UPSTREAM_VERSION@',
	author = "Noel O'Boyle",
	author_email = "baoilleach@gmail.com",
	maintainer = "debichem team",
	maintainer_email = "debichem-devel@lists.alioth.debian.org",
	url = "http://gausssum.sf.net",
	license = "GNU GPL",
	scripts = ['GaussSum.py'],
	packages = ['gausssum'],
	package_dir = {'gausssum': 'gausssum'},
	data_files = [
	    ('share/gausssum', ['mesh2.gif', 'mesh.gif']),
	    ('share/doc/gausssum/html', glob.glob('Docs/*'))
	]
)

